package com.onlinedistractiontracker.web;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.onlinedistractiontracker.domain.Badge;
import com.onlinedistractiontracker.domain.Trackings;
import com.onlinedistractiontracker.domain.UserAccount;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("userbadge")
@Controller
public class UserBadgeController {
	
	List<Badge> allBadgesList = Badge.findAllBadges();
	
	/**
	 * get Current Logged user
	 * 
	 * @return the current UserAccount object
	 */
	public UserAccount getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		return UserAccount.findUserAccountsByUserNameEquals(name).getSingleResult();

	}
	
	/**
	 * calculate score for a given user
	 * @return the score for the user
	 */
	public int calculateScore(UserAccount user) {
		List<Trackings> allTrackingRecords = Trackings.findAllTrackingses();
		int totalScore = 0;
		
		for (Trackings record : allTrackingRecords) {
			if (record.getUserId().getUserName().equals(user.getUserName())) {
				totalScore = totalScore + record.getDuration();
			}
		}
		return totalScore;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String sortUserByScore(Model model) {
		UserAccount currentUser = getCurrentUser();
		int currentUserScore = calculateScore(currentUser);
		Map<String, String> userBadgeMap = new TreeMap<String, String>();
		ArrayList<Badge> userBadgeList = new ArrayList<Badge>();
		Set<Badge> tempUserBadges = new HashSet<Badge>();
		for (Badge b : allBadgesList) {
			if (currentUserScore >= b.getRequirment()) {
				tempUserBadges.add(b);
				userBadgeMap.put(b.getBadgeName(), b.getBadgeDescription());
				userBadgeList.add(b);
			}
		}
		currentUser.setBadges(tempUserBadges);
		System.out.println(currentUser.getBadges());
		model.addAttribute("badgeList", userBadgeList);
		model.addAttribute("badgeMap", userBadgeMap);
		model.addAttribute("user", currentUser);
		model.addAttribute("userBadgesSet", currentUser.getBadges());
		return "userbadge";
	}
}
