// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.onlinedistractiontracker.web;

import com.onlinedistractiontracker.domain.Distractions;
import com.onlinedistractiontracker.domain.UserAccount;
import com.onlinedistractiontracker.web.DistractionsController;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

privileged aspect DistractionsController_Roo_Controller_Finder {
    
    @RequestMapping(params = { "find=ByUserId", "form" }, method = RequestMethod.GET)
    public String DistractionsController.findDistractionsesByUserIdForm(Model uiModel) {
        uiModel.addAttribute("useraccounts", UserAccount.findAllUserAccounts());
        return "distractionses/findDistractionsesByUserId";
    }
    
    @RequestMapping(params = "find=ByUserId", method = RequestMethod.GET)
    public String DistractionsController.findDistractionsesByUserId(@RequestParam("userId") UserAccount userId, Model uiModel) {
        uiModel.addAttribute("distractionses", Distractions.findDistractionsesByUserId(userId).getResultList());
        return "distractionses/list";
    }
    
}
