package com.onlinedistractiontracker.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FacebookAuthController {
	
	@RequestMapping(value = "/facebookAuth", method = RequestMethod.GET)
	public String facebookAuth(){
		System.out.println("this is facebookAuth!");
		return "facebooksignin";
	}
}
