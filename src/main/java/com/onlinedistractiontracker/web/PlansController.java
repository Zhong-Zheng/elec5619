package com.onlinedistractiontracker.web;
import com.onlinedistractiontracker.domain.Plans;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/planses")
@Controller
@RooWebScaffold(path = "planses", formBackingObject = Plans.class)
public class PlansController {
}
