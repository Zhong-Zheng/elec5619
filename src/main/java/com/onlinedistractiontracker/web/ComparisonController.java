package com.onlinedistractiontracker.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.onlinedistractiontracker.domain.Trackings;
import com.onlinedistractiontracker.domain.UserAccount;

@RequestMapping("/compare")
@Controller
public class ComparisonController {
	
//	private List<Trackings> allRecords = Trackings.findTrackingsesByUserId(getCurrentUser()).getResultList();
	public HashMap<String, Integer> calculateTotalDuration(
			List<Trackings> Records) {
		HashMap<String, Integer> domainDuration = new HashMap<String, Integer>();

		for (Trackings item : Records) {
			String domain = item.getDomain();
			int duration = 0;
			for (Trackings record : Records) {
				if (record.getDomain().equals(domain)) {
					duration += record.getDuration(); 
				}
//				String helper = "\'" + domain + "\'";
				domainDuration.put(domain, duration);
			}

		}
		return domainDuration;

	}
	public int calculateDurationByDomain(List<Trackings> records,String domain){
		int duration = 0;
		for(Trackings record : records){
			if(record.getDomain().equalsIgnoreCase(domain)){
				duration+=record.getDuration();
			}
		}
		return duration;
	}
	
	public ArrayList<Trackings> findTrackingsByDate(Date d,List<Trackings> records){
		ArrayList<Trackings> list = new ArrayList<Trackings>();
		for(Trackings t:records){
			if(sameDay(t.getStartTime().getTime(),d)){
				list.add(t);
			}
		}
		return list;
	}
	public ArrayList<String> findTopFiveDomain(List<Trackings> allRecords){
//		List<Trackings> allRecords = Trackings.findTrackingsesByUserId(getCurrentUser()).getResultList();
		HashMap<String,Integer> domainDuration = calculateTotalDuration(allRecords);
		ArrayList<String> results = new ArrayList<String>();
		for(int i =0;i<5;i++){
			String domain = findDomainWithMaxDuration(domainDuration);
			results.add(domain);
			domainDuration.remove(domain);
		}
		return results;
	}
	
	public String findDomainWithMaxDuration(HashMap<String,Integer> domainDuration){
		String max = null;
		for(String key:domainDuration.keySet()){
			if(max == null || domainDuration.get(key).compareTo(domainDuration.get(max)) > 0){
				max = key;
			}
		}
		return max;
	}

	public boolean sameDay(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
				&& cal1.get(Calendar.DAY_OF_YEAR) == cal2
						.get(Calendar.DAY_OF_YEAR);
		return sameDay;
	}
	
	public UserAccount getCurrentUser() {

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String name = auth.getName();
		return UserAccount.findUserAccountsByUserNameEquals(name).getSingleResult();

	}
	
	public HashMap<Date,ArrayList<Integer>> prepareData() throws ParseException{
		List<Trackings> allRecords = Trackings.findTrackingsesByUserId(getCurrentUser()).getResultList();
		HashMap<Date,ArrayList<Integer>> results = new HashMap<Date,ArrayList<Integer>>();
		List<Trackings> sortedRecords = sortTrackingByDate(allRecords);
		ArrayList<String> top5 = findTopFiveDomain(allRecords);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate =sdf.parse("2013-01-01");
		Date endDate = sdf.parse("2013-12-31");
		Calendar start = Calendar.getInstance();
		start.setTime(startDate);
		while(start.getTime().before(endDate)){
			start.add(Calendar.DATE, 1);
			ArrayList<Integer> duration = new ArrayList<Integer> ();
			ArrayList<Trackings> a = findTrackingsByDate(start.getTime(),sortedRecords);
			for(String domain:top5){

				duration.add(calculateDurationByDomain(a,domain));
			}
			
			results.put(start.getTime(), duration);
		}
	
		
		return results;
		
	}
	/**
	 * Sort the given Tracking arrayList by it's date
	 * @param sorted ArrayList
	 * @return
	 */
	public List<Trackings> sortTrackingByDate(List<Trackings> al) {
	    
		class DateComparator implements Comparator<Trackings> {

	        public int compare(Trackings t1, Trackings t2) {

	            return t1.getStartTime().compareTo(t2.getStartTime());
	        }
	    }
		Collections.sort(al, new DateComparator());
		return al;
	}
	
	public ArrayList<String> helper(ArrayList<String> a){
		ArrayList<String> c = new ArrayList<String>();
		for(String b : a){
			String helper = "\'" + b + "\'";
			c.add(helper);
		}
		return c;
	}
	@RequestMapping(method = RequestMethod.GET)
	public String comparison(Model model) throws ParseException{
		List<Trackings> allRecords = Trackings.findTrackingsesByUserId(getCurrentUser()).getResultList();
		List<Date> sortedKeys=new ArrayList(prepareData().keySet());
		Collections.sort(sortedKeys);
	
		model.addAttribute("keys",sortedKeys);
		model.addAttribute("hash", prepareData());
		model.addAttribute("top5", helper(findTopFiveDomain(allRecords)));
		return "compare";
	}
}
