package com.onlinedistractiontracker.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ConnectFacebookController {
	
	@RequestMapping(value = "/facebooksignin", method = RequestMethod.GET)
	public String connetFacebook(){
		System.out.println("this is ConnectFacebookController!");
		return "facebooksignin";
	}
}
