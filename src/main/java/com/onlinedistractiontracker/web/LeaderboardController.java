package com.onlinedistractiontracker.web;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.onlinedistractiontracker.domain.UserAccount;
import com.onlinedistractiontracker.domain.Trackings;

@RequestMapping("/leaderboard")
@Controller
public class LeaderboardController {
	
	List<UserAccount> allUsersList = UserAccount.findAllUserAccounts();

	/**
	 * calculate score for a given user
	 * @return the score for the user
	 */
	public int calculateScore(UserAccount user) {
		List<Trackings> allTrackingRecords = Trackings.findAllTrackingses();
		int totalScore = 0;
		
		for (Trackings record : allTrackingRecords) {
			if (record.getUserId().getUserName().equals(user.getUserName())) {
				totalScore = totalScore + record.getDuration();
			}
		}
		return totalScore;
	}
	
	/**
	 * update score for all users
	 */
	public void updateScore() {
		for (UserAccount userItem : allUsersList) {
			userItem.setScore(calculateScore(userItem));
		}
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String sortUserByScore(Model model) {
	    
		class ScoreComparator implements Comparator<UserAccount> {

	        public int compare(UserAccount c1, UserAccount c2) {

	            return new Integer(c1.getScore()).compareTo(new Integer(c2.getScore()));
	        }
	    }

	    System.out.println(allUsersList);
		updateScore();
		Collections.sort(allUsersList, new ScoreComparator());
		Collections.reverse(allUsersList);
		System.out.println(allUsersList);
		model.addAttribute("allUsersList", allUsersList);
		return "leaderboard";
	}
}
