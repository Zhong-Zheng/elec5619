package com.onlinedistractiontracker.web;
import com.onlinedistractiontracker.domain.SetPlan;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/setplans")
@Controller
@RooWebScaffold(path = "setplans", formBackingObject = SetPlan.class)
public class SetPlanController {
}
