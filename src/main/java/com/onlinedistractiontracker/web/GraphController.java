package com.onlinedistractiontracker.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.onlinedistractiontracker.domain.Trackings;
import com.onlinedistractiontracker.domain.UserAccount;

@RequestMapping("/charts")
@Controller
public class GraphController {

	/**
	 * calculate the total duration of all domains
	 * 
	 * @param todayRecords
	 * @return a HashMap which key is the domain, value is the duration
	 */
	public HashMap<String, Integer> calculateTotalDuration(
			List<Trackings> todayRecords) {
		HashMap<String, Integer> domainDuration = new HashMap<String, Integer>();

		for (Trackings item : todayRecords) {
			String domain = item.getDomain();
			int duration = 0;
			for (Trackings record : todayRecords) {
				if (record.getDomain().equals(domain)) {
					duration += record.getDuration() / 60;
				}
				String helper = "\'" + domain + "\'";
				domainDuration.put(helper, duration);
			}

		}
		return domainDuration;

	}

	public boolean sameDay(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
				&& cal1.get(Calendar.DAY_OF_YEAR) == cal2
						.get(Calendar.DAY_OF_YEAR);
		return sameDay;
	}

	/**
	 * get Current Logged user
	 * 
	 * @return the user name of the user
	 */
	public UserAccount getCurrentUser() {

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String name = auth.getName();
		return UserAccount.findUserAccountsByUserNameEquals(name).getSingleResult();

	}

	/**
	 * request GET, display today's chart
	 * 
	 * @param model
	 * @return chart
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String todayChart(Model model) {
		Calendar todayCal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		todayCal.set(Calendar.HOUR_OF_DAY, 0);
		todayCal.set(Calendar.MINUTE, 0);
		todayCal.set(Calendar.SECOND, 0);
		todayCal.set(Calendar.MILLISECOND, 0);
		Date today = todayCal.getTime();
		List<Trackings> allRecords = Trackings.findTrackingsesByUserId(getCurrentUser()).getResultList();
		List<Trackings> todayRecords = new ArrayList<Trackings>();
		for (Trackings record : allRecords) {

			if (sameDay(record.getStartTime().getTime(), today)) {
				todayRecords.add(record);
			}
		}
		model.addAttribute("user", getCurrentUser().getFirstName());
		model.addAttribute("hash", calculateTotalDuration(todayRecords));
		model.addAttribute("Day", dateFormat.format(today));
		return "chart";
	}

	/**
	 * Display chart of selected Date
	 * 
	 * @param model
	 * @param formDate
	 * @return chart
	 */
	@RequestMapping(method = RequestMethod.POST)
	public String dailyChart(Model model,
			@RequestParam("UserSelectedDate") String formDate) {

		List<Trackings> allRecords = Trackings.findTrackingsesByUserId(getCurrentUser()).getResultList();

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date userDate = sdf.parse(formDate);
			List<Trackings> recordOfTheDay = new ArrayList<Trackings>();
			for (Trackings record : allRecords) {
				if (sameDay(record.getStartTime().getTime(), userDate)) {
					recordOfTheDay.add(record);
				}
			}
			model.addAttribute("user", getCurrentUser().getFirstName());
			model.addAttribute("hash", calculateTotalDuration(recordOfTheDay));
			model.addAttribute("Day", formDate);

		} catch (ParseException ex) {
			ex.printStackTrace();
		}

		return "chart";
	}
}
