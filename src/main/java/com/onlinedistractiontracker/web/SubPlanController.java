package com.onlinedistractiontracker.web;
import com.onlinedistractiontracker.domain.SubPlan;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/subplans")
@Controller
@RooWebScaffold(path = "subplans", formBackingObject = SubPlan.class)
public class SubPlanController {
}
