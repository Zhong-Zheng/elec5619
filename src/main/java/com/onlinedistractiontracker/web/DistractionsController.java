package com.onlinedistractiontracker.web;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.onlinedistractiontracker.domain.Distractions;
import com.onlinedistractiontracker.domain.UserAccount;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;

@RequestMapping("/distractionses")
@Controller
@RooWebScaffold(path = "distractionses", formBackingObject = Distractions.class)
@RooWebFinder
public class DistractionsController {
    
    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("distractionses", Distractions.findDistractionsesByUserId(getAuthenticatedUser()).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) Distractions.findDistractionsesByUserId(getAuthenticatedUser()).getResultList().size() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	
            uiModel.addAttribute("distractionses", Distractions.findDistractionsesByUserId(getAuthenticatedUser()).getResultList());
        }
        return "distractionses/list";
    }
    
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Distractions distractions, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, distractions);
            return "distractionses/create";
        }
        uiModel.asMap().clear();
        distractions.setUserId(getAuthenticatedUser());
        distractions.persist();
        return "redirect:/distractionses/" + encodeUrlPathSegment(distractions.getId().toString(), httpServletRequest);
    }
    

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Distractions distractions, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, distractions);
            return "distractionses/update";
        }
    	
    	if(Distractions.findDistractions(distractions.getId()).getUserId().equals(getAuthenticatedUser())){
        	uiModel.asMap().clear();
        	distractions.setUserId(getAuthenticatedUser());
        	distractions.merge();
        }	
        return "redirect:/distractionses/" + encodeUrlPathSegment(distractions.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Distractions distractions = Distractions.findDistractions(id);
        //No one can delete someone elses
        if(distractions.getUserId().equals(getAuthenticatedUser())){
        	distractions.remove();
        	uiModel.asMap().clear();
        	uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        	uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        }	
        return "redirect:/distractionses";
    }
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        if(Distractions.findDistractions(id).getUserId().equals(getAuthenticatedUser())){
        	uiModel.addAttribute("distractions", Distractions.findDistractions(id));
        	uiModel.addAttribute("itemId", id);
        	return "distractionses/show";
        }else return "redirect:/distractionses";
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new Distractions());
        return "distractionses/create";
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        if(Distractions.findDistractions(id).getUserId().equals(getAuthenticatedUser())){
        	populateEditForm(uiModel, Distractions.findDistractions(id));
        	return "distractionses/update";
    	}else return "redirect:/distractionses";
    }
    
    public UserAccount getAuthenticatedUser(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); 
    	return UserAccount.findUserAccountsByUserNameEquals(name).getSingleResult();
    }
}
