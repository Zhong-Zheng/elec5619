package com.workplan.web;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.workplan.domain.SetPlan;
import com.workplan.domain.SubPlan;

@RequestMapping("/schedule/**")
@Controller
public class Schedule {

    @RequestMapping(method = RequestMethod.POST, value = "{id}")
    public void post(@PathVariable Long id, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
    }

    @SuppressWarnings("deprecation")
	@RequestMapping
    public String index(Model map) {
    	  	
    	SetPlan stp = new SetPlan();
    	SubPlan sbp = new SubPlan();
        long i = 0;
        int j = 0; 
        
        
        int countDays = 0;
        
        String pn = null;
       
        while(j == 0){
        if (stp.findSetPlan(i+1)!= null){
        	
        	SetPlan a = stp.findSetPlan(i+1);
		      int l = 0;
		      long k = 0;
		      
		      String spn = null;
		      String aled = null;
              while(l == 0){
            	   
            	   if(sbp.findSubPlan(k+1)!= null){
            		   SubPlan b = sbp.findSubPlan(k+1);
            		   if(b.getPlanName().getPlanName() == a.getPlanName()){
            			  if(spn == null) {
            			   spn = "SubPlanName: "+b.getSubPlanName() +" , ";
            			  }
            			  else{
            				  spn = spn + b.getSubPlanName() +" , ";
            			  }
            			 
            		   }
            		   
            	   }
            	   else{
            		   spn =spn +" </br>";
            		   l = 1;
            	   }
            	   k++;
              }   
               
            
              if(k!=0){
              int num = (int)k-1;
              long arraycount = 0;
              
             SubPlan[] arraylist = new SubPlan[num]; 
             while(arraycount < num){
            	 //if(sbp.findSubPlan(arraycount).getPlanName().getPlanName() == a.getPlanName()){
            	 arraylist[(int)arraycount] = sbp.findSubPlan(arraycount+1);
            	 //}
            	 arraycount++;
             }
        	int icon = 0;
        	SubPlan temp1;
        	while(icon == 0){
        		icon = 1;
        		for(int m = 0;m <num-1;m++){
        			if(arraylist[m].getPriority()>arraylist[m+1].getPriority()){
        				temp1 = arraylist[m];
        				arraylist[m] = arraylist[m+1];
        				arraylist[m+1] = temp1;
        				icon = 0 ;
        			}
        		}
        	}
        	aled = "Calculated schedule:  ";
        	for(int n = 0; n < num ; n++ ){
        		aled = aled + arraylist[n].getSubPlanName() + " , ";
        	}
        	aled = aled + " </br>";
             }
          	
        	if(pn == null){
        		pn = "PlanName: "+ a.getPlanName() + " <br/>" + spn + aled + " <br/>";
        	}
        	else{
        	pn = pn + "PlanName: "+ a.getPlanName() + " <br/>" + spn + aled + " <br/>";
        	}
        	i++;        	
        }
        else{
        	j = 1;
        }

       map.addAttribute("Plan", pn);
        }
                           	    	
        return "schedule/index";
    }
    

}
