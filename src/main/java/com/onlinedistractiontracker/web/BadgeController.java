package com.onlinedistractiontracker.web;
import com.onlinedistractiontracker.domain.Badge;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/badges")
@Controller
@RooWebScaffold(path = "badges", formBackingObject = Badge.class)
public class BadgeController {
}
