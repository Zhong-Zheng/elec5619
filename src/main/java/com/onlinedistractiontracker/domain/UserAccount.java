package com.onlinedistractiontracker.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

import org.springframework.beans.factory.annotation.Value;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findUserAccountsByUserNameAndPwdEquals", "findUserAccountsByUserNameEquals" })
public class UserAccount {

    /**
     */
    @NotNull
    @Column(unique = true)
    private String userName;

    /**
     */
    @NotNull
    @Size(min = 2)
    private String pwd;

    /**
     */
    @Column(unique = true)
    @Size(min = 2)
    private String facebookId;

    /**
     */
    @Size(min = 2)
    private String facebookToken;

    /**
     */
    @Size(min = 2)
    private String firstName;

    /**
     */
    @Size(min = 2)
    private String lastName;

    /**
     */
    @Size(min = 2)
    private String email;

    /**
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Badge> badges = new HashSet<Badge>();

    /**
     */
    @Value("0")
    private int score;

    /**
     */
    @Value("0")
    private int rank;

    /**
     */
    @Value("7")
    private int goal;

    /**
     */
    @Value("7")
    private int goalType;
}
