package com.onlinedistractiontracker.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import java.util.Calendar;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Events {

    /**
     */
    @ManyToOne
    private UserAccount userId;

    /**
     */
    @Size(min = 2)
    private String eventName;

    /**
     */
    @Size(min = 2)
    private String eventDescription;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar startTime;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar endTime;

    /**
     */
    @Size(min = 2)
    private String location;

    /**
     */
    private int privacy;

    /**
     */
    @Size(min = 2)
    private String picture;

    /**
     */
    @Size(min = 2)
    private String ticketUrl;
}
