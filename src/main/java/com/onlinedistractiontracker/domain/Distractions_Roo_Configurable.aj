// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.onlinedistractiontracker.domain;

import com.onlinedistractiontracker.domain.Distractions;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect Distractions_Roo_Configurable {
    
    declare @type: Distractions: @Configurable;
    
}
