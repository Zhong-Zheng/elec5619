package com.onlinedistractiontracker.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.Size;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Badge {

    /**
     */
    @Size(min = 2)
    private String badgeName;

    /**
     */
    @Size(min = 2)
    private String badgeDescription;

    /**
     */
    private int requirment;
}
