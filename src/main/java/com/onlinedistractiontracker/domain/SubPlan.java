package com.onlinedistractiontracker.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class SubPlan {

    /**
     */
    @ManyToOne
    private SetPlan PlanName;

    /**
     */
    @NotNull
    @Size(min = 2, max = 20)
    private String SubPlanName;

    /**
     */
    private Integer Priority;

    /**
     */
    private Integer DuringPeriod;
}
