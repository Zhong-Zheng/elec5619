// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.onlinedistractiontracker.domain;

import com.onlinedistractiontracker.domain.SubPlan;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect SubPlan_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager SubPlan.entityManager;
    
    public static final EntityManager SubPlan.entityManager() {
        EntityManager em = new SubPlan().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long SubPlan.countSubPlans() {
        return entityManager().createQuery("SELECT COUNT(o) FROM SubPlan o", Long.class).getSingleResult();
    }
    
    public static List<SubPlan> SubPlan.findAllSubPlans() {
        return entityManager().createQuery("SELECT o FROM SubPlan o", SubPlan.class).getResultList();
    }
    
    public static SubPlan SubPlan.findSubPlan(Long id) {
        if (id == null) return null;
        return entityManager().find(SubPlan.class, id);
    }
    
    public static List<SubPlan> SubPlan.findSubPlanEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM SubPlan o", SubPlan.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void SubPlan.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void SubPlan.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            SubPlan attached = SubPlan.findSubPlan(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void SubPlan.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void SubPlan.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public SubPlan SubPlan.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        SubPlan merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
