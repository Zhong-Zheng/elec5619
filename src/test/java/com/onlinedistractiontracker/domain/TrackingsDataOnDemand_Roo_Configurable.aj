// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.onlinedistractiontracker.domain;

import com.onlinedistractiontracker.domain.TrackingsDataOnDemand;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect TrackingsDataOnDemand_Roo_Configurable {
    
    declare @type: TrackingsDataOnDemand: @Configurable;
    
}
